package com.example.android.videoDemo;

import android.content.ContentValues;
import android.content.Context;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;

/**
 * Created by administrator on 16/12/2015.
 */
public class Utils {
    public static void addVideoToGallery(final String filePath,
                                         final Context context) {

        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Video.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
            values.put(MediaStore.MediaColumns.DATA, filePath);

            context.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String[] getFileList(String folderName){
        String[] fileList = null;
        File videoFiles = new File(folderName);

        if(videoFiles.isDirectory())
        {
            fileList=videoFiles.list();
        }
        if(fileList != null){
            for(int i=0;i<fileList.length;i++)
            {
                Log.e("Video:" + i + " File name", fileList[i]);
            }
        }
        return fileList;
    }
}
