package com.example.android.videoDemo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.android.camera2video.R;

import java.io.File;

public class VideoViewAvtivity extends Activity {

	private VideoView myVideoView;
	private MediaController mediaControls;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.video_view);
		if (mediaControls == null) {
			mediaControls = new MediaController(VideoViewAvtivity.this);
		}

		myVideoView = (VideoView) findViewById(R.id.video_view);
		try {
			myVideoView.setMediaController(mediaControls);
			Intent intent = getIntent();
			String path = intent.getStringExtra("path");
			myVideoView.setVideoURI(Uri.fromFile(new File(path)));
			myVideoView.start();

		} catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		}
		myVideoView.requestFocus();

	}

}
